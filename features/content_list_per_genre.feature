Feature: Listado de contenido por genero

  Background:
    Given I have logged in

  Scenario: View movies group by genre
    Given the following movies:
      | title               | stars | poster             | genre  | price |
      | El Origen           |  5    | el-origen.png      | Acción |  2    |
      | Ciudad de Dios      |  9    | cidade-de-deus.png | Drama  |  0    |
      | Atrapado sin salida |  9    | cidade-de-deus.png | Drama  | 10    |
      | Tiempos violentos   |  8    | pulp-fiction.png   | Acción |  5    |
     When I go to the list of available content
      And I click on "Agrupar por Género"
     Then I should see "Películas de Acción"
      And I should see "Películas de Drama"
