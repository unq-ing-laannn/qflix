Feature: Compra de QPoints

  Scenario: QPoints purchase
    Given I have logged in
      And I have 100 QPoints
     When I go to my profile
      And I fill in "qpoints" with 500
      And I click on "Comprar QPoints"
     Then I should have 600 QPoints
