Feature: Compra de películas

  Background:
    Given I have logged in

  Scenario Outline: Movie purchase
    Given a movie called "<title>"
     When I go to movies list
      And I click on "<title>"
      And I click on "Comprar película"
#      And I click on "Perfil"
#     Then I should see the movie "<title>" in my purchased list

	Examples:
 	 | title               |
	 | El Origen           |
	 | Ciudad de Dios      |
	 | Atrapado sin salida |
	 | Tiempos violentos   |
