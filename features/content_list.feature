Feature: Listado de contenido en detalle

  Background:
    Given I have logged in

  Scenario Outline: View movie info
    Given a movie called "<title>"
      And a movie with <stars> stars
      And a movie with poster "<poster>"
      And a movie with genre "<genre>"
      And a movie that cost <price> QPoints
     When I go to the list of available content
     Then I should see the title of movie
      And I should see the stars qualification of movie
      And I should see the poster of movie
      And I should see the genre of movie

    Examples:
      | title               | stars | poster             | genre  | price |
      | El Origen           |  5    | el-origen.png      | Action |  2    |
      | Ciudad de Dios      |  5    | cidade-de-deus.png | Drama  |  0    |
      | Atrapado sin salida |  3    | cidade-de-deus.png | Drama  | 10    |
      | Tiempos violentos   |  1    | pulp-fiction.png   | Action |  5    |
