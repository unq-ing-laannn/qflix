#encoding: utf-8

When(/^I enter to the content$/) do
	visit movie_path(@movie.id)
end

Then(/^I have a play button$/) do
	expect(page).to have_content('Ver Película')
end

Then(/^I go the play screen$/) do
	click_link 'Ver Película'
	expect(page).to have_selector('iframe')
end