Given(/^I see the navbar search form$/) do
  expect(page).to have_selector('.navbar form')
end

Given(/^I have a movie with "([^"]*)" title, "([^"]*)" synopsis and "([^"]*)" genre$/) do |title, synopsis, genre|
  @movie.title = title
  @movie.synopsis = synopsis
  @movie.genre = genre
  @movie.save!
end

When(/^I fill the search input with "([^"]*)"$/) do |text|
  @search_text = text
  fill_in 'search-input', with: text
end

When(/^I click the search button$/) do
  click_button('search-button')
end

Then(/^I should see the text "([^"]*)"$/) do |text|
  expect(page).to have_content(text)
end

Then(/^I should see the movie "([^"]*)"$/) do |title|
  expect(page).to have_css(".movie h4 a", text: title)
end