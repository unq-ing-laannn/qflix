#encoding: utf-8

Given(/^a movie with title "([^"]*)" and trailer "([^"]*)"$/) do |title, url_trailer|
  @movie.title = title
  @movie.url_trailer = url_trailer
  @movie.save!
end

When(/^I go to movies list$/) do
  visit movies_path
end

When(/^I go to movie's trailer$/) do
  expect(page).to have_content(@movie.title)
  visit movie_trailer_path(@movie.id)
end

Then(/^I should see the trailer of the movie$/) do
  expect(page).to have_selector("iframe[src='https://www.youtube.com/embed/"+@movie.url_trailer+"']")
end