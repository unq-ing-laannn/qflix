FactoryGirl.define do

    factory :movie do
        title 'Title Movie'
        year '2000'
        synopsis 'Synopsis Movie'
        poster 'poster.png'
        genre 'Genre'
        stars 5
        price 7
        url_movie 'http://url.movie.com/'
        url_trailer 'http://url.trailer.com/'
    end

end