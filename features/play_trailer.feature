Feature: Reproducir trailer correspondiente a la pelicula

  Background:
    Given I have logged in

  Scenario Outline: Trailer reproduction
    Given a movie with title "<title>" and trailer "<trailer>"
    When I go to movies list
    And I go to movie's trailer
    Then I should see the trailer of the movie


	Examples:
 	 | title               | trailer     |
	 | El Origen           | RyfbchpdJ6U |
	 | Ciudad de Dios      | zyzJj9lF1PU |
	 | Atrapado sin salida | 1qMFh22qGyg |
	 | Tiempos violentos   | DkgTIjkVrY8 |