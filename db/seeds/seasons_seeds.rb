@shseason1 = Season.create(
    title: 'Temporada 1',
    poster: '26.jpg',
    stars: 5,
    url_trailer: 'JP5Dr63TbSU',
    serie_id: @sherlock.id
)

@shseason2 = Season.create(
    title: 'Temporada 2',
    poster: '27.jpg',
    stars: 5,
    url_trailer: 'vydmN9pzDzg',
    serie_id: @sherlock.id
)

@shseason3 = Season.create(
    title: 'Temporada 3',
    poster: '28.jpg',
    stars: 5,
    url_trailer: '9UcR9iKArd0',
    serie_id: @sherlock.id
)

@shseason4 = Season.create(
    title: 'Temporada 4',
    poster: '25.jpg',
    stars: 5,
    url_trailer: 's0fGZKCeSz8',
    serie_id: @sherlock.id
)

@tdseason1 = Season.create(
    title: 'Temporada 1',
    poster: '29.jpg',
    stars: 5,
    url_trailer: 'mXG1netn9_g',
    serie_id: @truedetective.id
)

@tdseason2 = Season.create(
    title: 'Temporada 2',
    poster: '29.jpg',
    stars: 5,
    url_trailer: 'mXG1netn9_g',
    serie_id: @truedetective.id
)

@mrseason1 = Season.create(
    title: 'Temporada 1',
    poster: '30.jpg',
    stars: 4,
    url_trailer: 'U94litUpZuc',
    serie_id: @mrrobot.id
)

@bobseason1 = Season.create(
    title: 'Temporada 1',
    poster: '31.jpg',
    stars: 5,
    url_trailer: '1wlYPlwjGOY',
    serie_id: @bandofbrothers.id
)

@lseason1 = Season.create(
    title: 'Temporada 1',
    poster: '32.jpg',
    stars: 4,
    url_trailer: 'X4bF_quwNtw',
    serie_id: @lucifer.id
)

@jjtemporda1 = Season.create(
    title: 'Temporada 1',
    poster: '33.jpg',
    stars: 4,
    url_trailer: 'nWHUjuJ8zxE',
    serie_id: @jessicajones.id
)