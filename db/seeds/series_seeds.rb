@sherlock = Serie.create(
    title: 'Sherlock',
    year: '2010',
    synopsis: 'Una actualización moderna encuentra al famoso detective y su compañero médico de la solución de la delincuencia en el Londres del siglo 21.',
    poster: '24.jpg',
    genre: 'Crime',
    stars: 5,
    price: 100,
    url_trailer: '7hjPxUfV32Q'
)

@truedetective = Serie.create(
    title: 'True Detective',
    year: '2014',
    synopsis: 'Dos detectives de Lousiana vuelven a investigar el difícil caso de un asesino en serie en el que ya habían trabajado.',
    poster: '29.jpg',
    genre: 'Crime',
    stars: 5,
    price: 90,
    url_trailer: '7hjPxUfV32Q'
)

@mrrobot = Serie.create(
    title: 'Mr. Robot',
    year: '2015',
    synopsis: 'Sigue un joven programador de computadoras que sufre de trastorno de ansiedad y forma conexiones social a través de la piratería. Es reclutado por un anarquista misterioso, que se hace llamar Mr. Robot.',
    poster: '30.jpg',
    genre: 'Drama',
    stars: 4,
    price: 100,
    url_trailer: 'U94litUpZuc'
)

@bandofbrothers = Serie.create(
    title: 'Band of Brothers',
    year: '2001',
    synopsis: 'La historia de la Compañía Easy del ejército americano, división aerea 101 y su misión en Europa II Guerra Mundial de la Operación Overlord través del dia V-J.',
    poster: '31.jpg',
    genre: 'Drama',
    stars: 5,
    price: 100,
    url_trailer: '1wlYPlwjGOY'
)

@lucifer = Serie.create(
    title: 'Lucifer',
    year: '2016',
    synopsis: 'Satanás establece su residencia en Los Ángeles.',
    poster: '32.jpg',
    genre: 'Drama',
    stars: 4,
    price: 100,
    url_trailer: 'X4bF_quwNtw'
)

@jessicajones = Serie.create(
    title: 'Marvel - Jessica Jones',
    year: '2015',
    synopsis: 'Una ex superhéroe decide reiniciar su vida al convertirse en una investigadora privada.',
    poster: '33.jpg',
    genre: 'Action',
    stars: 4,
    price: 100,
    url_trailer: 'nWHUjuJ8zxE'
)