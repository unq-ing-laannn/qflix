class CreateSeasons < ActiveRecord::Migration
  def change
    create_table :seasons do |t|
      t.string  :title
      t.string  :poster
      t.integer  :stars
      t.integer :season_id
      t.string :url_trailer
      t.belongs_to :serie

      t.timestamps null: false
    end
  end
end
