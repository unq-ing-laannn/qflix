class SeasonsController < ApplicationController

  def index
    @seasons = Season.all
  end

  def show
    @season = Season.find(params[:id])
    @chapters = @season.chapters
  end

  def play
  end

  def trailer
    @season = Season.find(params[:id])
  end

  def search
    @search_text = params[:busqueda]
    @seasons = Season.search(@search_text)
  end

  def pay
    @user.qpoints -= params[:qpoints].to_i
    @user.save!
    redirect_to :back
  end

end
