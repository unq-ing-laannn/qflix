class ChaptersController < ApplicationController

  def index
    @chapters = Chapter.all
  end

  def show
    @chapter = Chapter.find(params[:id])
  end

  def play
  end

  def trailer
    @chapter = Chapter.find(params[:id])
  end

  def search
    @search_text = params[:busqueda]
    @chapters = Chapter.search(@search_text)
  end

  def pay
    @user.qpoints -= params[:qpoints].to_i
    @user.save!
    redirect_to :back
  end

end
