class MoviesController < ApplicationController
  def index
    @movies = Movie.all
  end

  def show
    @movie = Movie.find(params[:id])
  end

  def play
    @movie = Movie.find(params[:id])
  end

  def trailer
    @movie = Movie.find(params[:id])
  end

  def search
    @search_text = params[:busqueda]
    @movies = Movie.search(@search_text)
  end

  def order
    @movies = Movie.all
    @genres = @movies.map { |movie| movie.genre }.to_set
  end

  def buy
    @user = User.find(1)
    @movie = Movie.find(params[:id])
    if !@user.movies.include?(@movie)
      @user.qpoints -= @movie.price
      @user.movies << @movie
      @user.save!
    end
    redirect_to :back
  end

end
