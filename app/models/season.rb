class Season < ActiveRecord::Base

  belongs_to :serie
  has_many :chapters

  def self.search(search)
    where("lower(title) LIKE ? or lower(synopsis) LIKE ? or lower(genre) LIKE ?", "%#{search}%".downcase, "%#{search}%".downcase, "%#{search}%".downcase)
  end

end
