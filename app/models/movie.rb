class Movie < ActiveRecord::Base

	has_and_belongs_to_many :users

	def self.search(search)
	  where("lower(title) LIKE ? or lower(synopsis) LIKE ? or lower(genre) LIKE ?", "%#{search}%".downcase, "%#{search}%".downcase, "%#{search}%".downcase)
	end
	
end
